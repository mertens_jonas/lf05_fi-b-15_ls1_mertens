import java.util.Scanner;
public class Rom {
    public static void main(String[] args) {
        int ausgabe;
        Scanner tastatur = new Scanner(System.in);
        System.out.println("Welches römische Zahlzeichen soll umgewandelt werden?");
        char zahlZeichen = tastatur.next().charAt(0);
        switch(zahlZeichen)
        {
            case 'I':
                ausgabe = 1;
                break;
            case 'V':
                ausgabe = 5;
                break;
            case 'X':
                ausgabe = 10;
                break;
            case 'L':
                ausgabe = 50;
                break;
            case 'C':
                ausgabe = 100;
                break;
            case 'D':
                ausgabe = 500;
                break;
            case 'M':
                ausgabe = 1000;
                break;
            default:
                System.out.println("Das ist kein gültiges römisches Zahlzeichen!");
                return;
        }
        System.out.println(ausgabe);
    }
}
