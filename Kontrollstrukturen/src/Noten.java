import java.util.Scanner;
public class Noten {
    public static void main(String[] args) {
        String ausgabe = "";
        Scanner tastatur = new Scanner(System.in);
        System.out.println("Welche Note soll umgewandelt werden?");
        int note = tastatur.nextInt();
        switch(note)
        {
            case 1:
                ausgabe = "Sehr Gut";
                break;
            case 2:
                ausgabe = "Gut";
                break;
            case 3:
                ausgabe = "Befriedigend";
                break;
            case 4:
                ausgabe = "Ausreichend";
                break;
            case 5:
                ausgabe = "Mangelhaft";
                break;
            case 6:
                ausgabe = "Ungenügend";
                break;
            default:
                ausgabe = "Das ist keine gültige Note!";
        }
        System.out.println(ausgabe);
    }
}
