public class Rabattsystem {
    public static void main(String[] args) {
        double bestellWert = 700;
        double rabatt = 0;
        if (bestellWert <= 100) {
            System.out.println("Bestellwert kleiner als 100€.");
            rabatt = 10;

        } else if (bestellWert > 100 && bestellWert <= 500) {
            System.out.println("Bestellwert zwischen 100€ und 500€.");
            rabatt = 15;

        } else if (bestellWert > 500) {
            System.out.println("Bestellwert größer als 500€.");
            rabatt = 20;
        }

        double prozentWert = bestellWert * 19 / 100;
        double rabattWert = (bestellWert + prozentWert) * rabatt / 100;
        double ergebnis = (bestellWert + prozentWert) - rabattWert;
        System.out.println("Ergebnis: " + ergebnis);
    }
}
