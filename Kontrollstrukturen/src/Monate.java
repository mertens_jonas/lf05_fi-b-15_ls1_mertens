import java.util.Scanner;
public class Monate {
    public static void main(String[] args) {
        String ausgabe = "";
        Scanner tastatur = new Scanner(System.in);
        System.out.println("Welcher Monat soll umgewandelt werden?");
        int monat = tastatur.nextInt();
        switch(monat)
        {
            case 1:
                ausgabe = "Januar";
                break;
            case 2:
                ausgabe = "Februar";
                break;
            case 3:
                ausgabe = "März";
                break;
            case 4:
                ausgabe = "April";
                break;
            case 5:
                ausgabe = "Mai";
                break;
            case 6:
                ausgabe = "Juni";
                break;
            case 7:
                ausgabe = "Juli";
                break;
            case 8:
                ausgabe = "August";
                break;
            case 9:
                ausgabe = "September";
                break;
            case 10:
                ausgabe = "Oktober";
                break;
            case 11:
                ausgabe = "November";
                break;
            case 12:
                ausgabe = "Dezember";
                break;
            default:
                ausgabe = "Das ist kein gültiger Monat!";
        }
        System.out.println(ausgabe);
    }
}