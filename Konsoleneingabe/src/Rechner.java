import java.util.Scanner; // Import der Klasse Scanner
public class Rechner
{

	public static void main(String[] args) // Hier startet das Programm
	{

		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Hallo User,\nbitte geben Sie ihren Vornamen an: ");
		
		// Die Variable zahl1 speichert die erste Eingabe
		String name = myScanner.next();
		
		System.out.print("Bitte geben Sie ihr Alter an: ");
		
		// Die Variable zahl2 speichert die zweite Eingabe
		int alter = myScanner.nextInt();
		
		System.out.print("\n\nHallo " + name + ",\nsie haben mir gesagt das sie " + alter + " Jahre alt sind.");
		myScanner.close();

	}
}