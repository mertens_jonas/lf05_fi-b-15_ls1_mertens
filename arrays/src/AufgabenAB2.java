public class AufgabenAB2 {
    public static void main(String[] args) {
        arrayHelper();
        aufgabeZwei();
    }

    public static void arrayHelper(){
        System.out.println("Aufgabe 1:\n");
        int[] zahlen = {3, 7, 12, 18, 37, 42};
        String output = convertArrayToString(zahlen);
        System.out.println(output);
    }

    public static String convertArrayToString(int[] zahlen){
        String a = "";
        for (int i = 0; i < zahlen.length; i++){
            a = a + Integer.toString(zahlen[i]);
            if(!(i + 1 == zahlen.length)){
                a = a + ", ";
            }
        }
        return a;
    }
    public static void aufgabeZwei(){
        int[] arrayAufgabeZwei = {10, 9, 8, 5, 1};
        System.out.println("\nAufgabe 2:\n");
        System.out.print("Bevor: ");
        for(int i = 0; i < arrayAufgabeZwei.length; i++) {
            System.out.print(arrayAufgabeZwei[i] + " ");
        }
        arrayAufgabeZwei = turnArray(arrayAufgabeZwei);
        System.out.print("\n\nDanach: ");
        for(int i = 0; i < arrayAufgabeZwei.length; i++){
            System.out.print(arrayAufgabeZwei[i] + " ");
        }
    }

    public static int[] turnArray(int[] zahlen) {
        int temp;
        for (int i = 0; i < (zahlen.length/2); i++){
            temp = zahlen[i];
            zahlen[i] = zahlen[zahlen.length -1 -i];
            zahlen[zahlen.length -1 -i] = temp;
        }
        return zahlen;

    }
}
