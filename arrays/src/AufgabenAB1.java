import java.util.Scanner;
public class AufgabenAB1 {
    public static void main(String[] args) {
        ungeradeZahlen();
        palindrom();
        lotto();
    }
    public static void ungeradeZahlen(){
        System.out.println("\nAufgabe 2:\n");
        int[] zahlenfeld = new int[10];
        int x = 0;
        for (int i = 1; i < 20; i++){
            boolean divisible = i % 2 == 0;
            if (!divisible){
                zahlenfeld[x] = i;
                x++;
            }
        }
        for (int i = 0; i < zahlenfeld.length; i++){
            System.out.println(zahlenfeld[i]);
        }
    }

    public static void palindrom(){
        System.out.println("\nAufgabe 3:\n");
        Scanner tastatur = new Scanner(System.in);
        char[] zeichenfeld = new char[5];
        char zeichen;
        for (int i = 0; i < zeichenfeld.length; i++){
            System.out.print("Geben sie das " + (i + 1) + ". Zeichen ein: ");
            zeichen = tastatur.next().charAt(0);
            zeichenfeld[i] = zeichen;
        }

        System.out.println("\nIn verkehrter Reihenfolge:");
        for (int i = 4; i >= 0; i--){
            System.out.print(zeichenfeld[i]);
        }
    }

    public static void lotto(){
        System.out.println("\n\nAufgabe 4:\n");
        int[] lottofeld = {3, 7, 12, 18, 37, 42};
        System.out.print("[ ");
        for (int i = 0; i < lottofeld.length; i++){
            System.out.print(lottofeld[i] + " ");
        }
        System.out.print("]\n\n");
        int[] checkzahlen = {12, 13};
        boolean hit;
        for (int i = 0; i < checkzahlen.length; i++){
            hit = false;
            for (int x = 0; x < lottofeld.length; x++){
                if(checkzahlen[i]==lottofeld[x]){
                    System.out.println("Die Zahl " + checkzahlen[i] + " ist in der Ziehung enthalten.");
                    hit = true;
                }
            }
            if(!hit){
                System.out.println("Die Zahl " + checkzahlen[i] + " ist nicht in der Ziehung enthalten.");
            }
        }
    }
}
