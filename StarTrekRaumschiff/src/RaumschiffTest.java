public class RaumschiffTest {
    public static void main(String[] args) {

        Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2);
        klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
        klingonen.addLadung(new Ladung("bat'leth Klingonen Schwert", 200));

        Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
        romulaner.addLadung(new Ladung("Borg-Schrott", 5));
        romulaner.addLadung(new Ladung("Rote Materie", 2));
        romulaner.addLadung(new Ladung("Plasma-Waffe", 50));

        Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni'Var", 5);
        vulkanier.addLadung(new Ladung("Forschungssonde", 35));
        vulkanier.addLadung(new Ladung("Photonentorpedo", 3));
        vulkanier.ladungAusgeben();
        vulkanier.logbuchAusgeben();
    }
}
