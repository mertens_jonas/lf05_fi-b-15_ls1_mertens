import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class Raumschiff {
    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffname;
    private static final ArrayList<String> broadcastKommunikator = new ArrayList<>();
    private final ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();

    public Raumschiff() {

    }

    public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffname, int androidenAnzahl) {

        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.schiffname = schiffname;
        this.androidenAnzahl = androidenAnzahl;
    }

    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {

        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {

        return energieversorgungInProzent;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {

        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public int getSchildeInProzent() {

        return schildeInProzent;
    }

    public void setSchildeInProzent(int schildeInProzent) {

        this.schildeInProzent = schildeInProzent;
    }

    public int getHuelleInProzent() {

        return huelleInProzent;
    }

    public void setHuelleInProzent(int huelleInProzent) {

        this.huelleInProzent = huelleInProzent;
    }

    public int getLebenserhaltungssystemeInProzent() {

        return lebenserhaltungssystemeInProzent;
    }

    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {

        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    public int getAndroidenAnzahl() {

        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {

        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffname() {

        return schiffname;
    }

    public void setSchiffname(String schiffname) {

        this.schiffname = schiffname;
    }

    public void addLadung(Ladung neueLadung) {

        this.ladungsverzeichnis.add(neueLadung);
    }

    public void photonentorpedoSchiessen(Raumschiff raumschiff) {
        if (this.photonentorpedoAnzahl == 0) {
            nachrichAnAlle(this.schiffname, "-=*Click*=-");
        } else {
            this.photonentorpedoAnzahl--;
            nachrichAnAlle(this.schiffname, "Photonentorpedo abgeschossen");
            treffer(raumschiff.getSchiffname());
            raumschiff.schaden();
        }
    }

    public void phaserkanoneSchiessen(Raumschiff raumschiff) {
        if (this.energieversorgungInProzent < 50) {
            nachrichAnAlle(this.schiffname, "-=*Click*=-");
        } else {
            this.energieversorgungInProzent = this.energieversorgungInProzent / 2;
            nachrichAnAlle(this.schiffname, "Phaserkanone abgeschossen");
            treffer(raumschiff.getSchiffname());
            raumschiff.schaden();
        }
    }

    public void nachrichAnAlle (String schiffname, String message) {
        Raumschiff.broadcastKommunikator.add(schiffname + ": " + message);
    }

    public static ArrayList<String> eintrageLogbuchZurueckgeben() {

        return Raumschiff.broadcastKommunikator;
    }

    public void photonentorpedoLaden(int anzahlTorpedos) {
        AtomicBoolean fertig = new AtomicBoolean(false);
        this.ladungsverzeichnis.forEach(i -> {if (i.getBezeichnung().equals("Photonentorpedo") && i.getMenge() > 0 && !fertig.get()) {
            fertig.set(true);
        }});
    }

    public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {

    }

    public void treffer(String schiffname) {
        System.out.printf("%s wurde getroffen!%n", schiffname);
        nachrichAnAlle(schiffname, "wurde getroffen");
    }

    public void schaden() {
        if (this.schildeInProzent > 0) {
            this.schildeInProzent = this.schildeInProzent / 2;
        }
        if (this.schildeInProzent == 0) {
            this.energieversorgungInProzent = this.energieversorgungInProzent / 2;
            this.huelleInProzent = this.huelleInProzent / 2;
        }
        if (this.huelleInProzent == 0) {
            this.lebenserhaltungssystemeInProzent = 0;
            nachrichAnAlle(this.getSchiffname(), "Die Lebenserhaltungssysteme wurden vernichtet!");
        }
    }
    public void statusAusgeben() {
        System.out.printf("Schiffname: %s%nLebenserhaltungssysteme: %s%%%nSchilde: %s%%%nHülle: %s%%%nEnergieversorgung: %s%%%nAndroiden: %s%nPhotonentorpedos: %s%n",
                this.schiffname,
                this.lebenserhaltungssystemeInProzent,
                this.schildeInProzent,
                this.huelleInProzent,
                this.energieversorgungInProzent,
                this.androidenAnzahl,
                this.photonentorpedoAnzahl
        );
    }

    public void ladungAusgeben() {

        if (this.ladungsverzeichnis.isEmpty()) {
            System.out.printf("Das Schiff: %s, hat keine Ladung!%n%n", this.schiffname);
        } else {
            System.out.printf("Ladung des Schiffs: %s%n%n", this.schiffname);
            this.ladungsverzeichnis.forEach(i -> System.out.printf("Ladung: %s%nMenge: %d%n%n", i.getBezeichnung(), i.getMenge()));
        }
    }

    public void logbuchAusgeben() {
        System.out.printf("Logbuch Einträge:%n%n");
        System.out.print(Raumschiff.broadcastKommunikator);
        Raumschiff.broadcastKommunikator.forEach(i -> System.out.printf("%s", i));
    }
}
