import java.util.Scanner; // Import der Klasse Scanner
public class Quadrieren {
   
	public static void main(String[] args) {
		
		titel();
		
		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		double x = eingabe();
		
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis = rechnen(x);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		ausgabe(ergebnis, x);
		
	}	
	public static void titel() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
	}
	
	public static double eingabe() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte eine Zahl eingeben: ");
		double x = tastatur.nextDouble();
		tastatur.close();
		return x;
	}
	
	public static double rechnen(double x) {
		double ergebnis= x * x;
		return ergebnis;
	}
	
	public static void ausgabe(double ergebnis, double x) {
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}
	

}
