
public class Volumenberechnung {
	public static void main(String[] args) {
		double a = 1.0;
		double b = 2.0;
		double c = 3.0;
		double h = 4.0;
		double r = 2.0;
		wuerfel(a);
		quader(a, b, c);
		pyramide(a, h);
		kugel(r);
	}
	public static void wuerfel(double a) {
		double vwuerfel = a * a * a;
		System.out.printf("Das Volumen des W�rfels mit einer Seitenl�nge von: %.2fcm ist %.2fcm�\n", a, vwuerfel);
	}
	public static void quader(double a, double b, double c) {
		double vquader = a * b * c;
		System.out.printf("Das Volumen des Quaders mit den Seitenl�nge von: %.2fcm, %.2fcm und %.2fcm ist %.2fcm�\n", a, b, c, vquader);
	}
	public static void pyramide(double a, double h) {
		double vpyramide = a * a * h / 3;
		System.out.printf("Das Volumen der Pyramide mit einer Seitenl�nge von: %.2fcm und einer H�he %.2fcm betr�gt: %.2fcm�\n", a, h, vpyramide);
	}
	public static void kugel(double r) {
		double vkugel = 4 / 3 * (r * r * r) * Math.PI;
		System.out.printf("Das Volumen der Kugel mit einem Radius von: %.2fcm betr�gt %.2fcm�\n",r , vkugel);
	}
}
