public class Argumente {
	
	public static void main(String[] args) {
		
		ausgabe(1, "Mana");
		ausgabe(2, "Elise");
		ausgabe(3, "Johanna");
		ausgabe(4, "Felizitas");
		ausgabe(5, "Karla");
		System.out.println(vergleichen(1, 2));
		System.out.println(vergleichen(1, 5));
		System.out.println(vergleichen(3, 4));
		double x = 2.36;
		double y = 7.87;
		double ergebnis = multiplikation(x, y);
		System.out.printf("Das Ergebnis der Multiplikation von %.2f und %.2f hat das Ergebnis: %.4f", x, y, ergebnis);
	}

	public static void ausgabe(int zahl, String name) {
		System.out.println(zahl + ": " + name);
	}

	public static boolean vergleichen(int arg1, int arg2) {
		return (arg1 + 8) < (arg2 * 3);
	}
	
	public static double multiplikation(double x, double y) {
		double ergebnis = x * y; 
		return ergebnis;
	}
}