import java.util.Scanner;
public class Train3 {
    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);
        int fahrzeit = 8; // Fahrzeit: Berlin Hbf -> Spandau

        System.out.println("Wird in Spandau gehalten? (j/n)");
        char spandau = myScanner.next().charAt(0);

        if (spandau == 'j') {
            fahrzeit = addfahrzeit(fahrzeit, 2);
        }

        System.out.println("Fahren sie nach Hamburg? (j/n)");
        char richtungHamburg = myScanner.next().charAt(0);

        if (richtungHamburg == 'j') {
            fahrzeit = addfahrzeit(fahrzeit, 96);
            zielausgabe("Hamburg" , fahrzeit);

        } else {
            fahrzeit = addfahrzeit(fahrzeit, 34);
            System.out.println("Halten sie in Stendal? (j/n)");
            char haltInStendal = myScanner.next().charAt(0);

            if (haltInStendal == 'j') {
                fahrzeit = addfahrzeit(fahrzeit,16);

            } else {
                fahrzeit = addfahrzeit(fahrzeit,6);
            }

            System.out.println("Wohin fahren sie? (w - Wolfsburg /b - Braunschweig /h - Hannover)");
            char endetIn = myScanner.next().charAt(0);

            if (endetIn == 'w') {
                fahrzeit = addfahrzeit( fahrzeit, 29);
                zielausgabe("Wolfsburg" , fahrzeit);

            } else if (endetIn == 'b') {
                fahrzeit = addfahrzeit( fahrzeit, 50);
                zielausgabe("Braunschweig" , fahrzeit);

            } else if (endetIn == 'h'){
                fahrzeit = addfahrzeit( fahrzeit, 62);
                zielausgabe("Hannover" , fahrzeit);

            } else {
                System.out.println("Sie sind entgleist! :(");
            }
        }
    }
    public static int addfahrzeit (int fahrzeit, int addzeit) {
        fahrzeit = fahrzeit + addzeit;
        return fahrzeit;
    }

    public static void zielausgabe (String Ort, int fahrzeit) {
        System.out.println("Sie erreichen "+ Ort + " nach " + fahrzeit + " Minuten.");
    }
}