import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double finalerGesamtbetrag = fahrkartenbestellungErfassen(tastatur);

		// Geldeinwurf
		// -----------
		double rueckgabebetrag = fahrkartenBezahlen(finalerGesamtbetrag, tastatur);
		

		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();
		

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		rueckgeldAusgeben(rueckgabebetrag);


		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
		tastatur.close();
	}

	public static double fahrkartenbestellungErfassen(Scanner tastatur) {
		double zuZahlenderBetrag;
		double finalerGesamtbetrag;
		System.out.print("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();
		zuZahlenderBetrag = ticketPreisValidieren(zuZahlenderBetrag);
		System.out.print("Anzahl der Tickets: ");
		int ticketAnzahl = tastatur.nextInt();
		ticketAnzahl = ticketAnzahlValidieren(ticketAnzahl);
		finalerGesamtbetrag = (ticketAnzahl * zuZahlenderBetrag);
		return finalerGesamtbetrag;

	}

	public static double fahrkartenBezahlen(double finalerGesamtbetrag, Scanner tastatur) {
		double eingeworfeneMuenze;
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < finalerGesamtbetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (finalerGesamtbetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		double rueckgabebetrag = eingezahlterGesamtbetrag - finalerGesamtbetrag;
		return rueckgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		int millisekunde = 250;
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(millisekunde);
		}
		System.out.println("\n");
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {
		if (rueckgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", rueckgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabebetrag -= 0.05;
			}
		}
	}
	static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	static void muenzeAusgeben(int betrag, String einheit) {

		System.out.println(betrag + " " + einheit);
	}

	public static int ticketAnzahlValidieren(int ticketAnzahl) {
		if (ticketAnzahl > 10 || ticketAnzahl < 1) {
			ticketAnzahl = 1;
			System.out.println("\nEs können nur zwischen 1 und 10 Tickets auf einmal bestellt werden!\n" +
					"Daher wurde ihre Bestellung auf 1 Ticket geändert!\n");
		}
		return ticketAnzahl;
	}

	public static double ticketPreisValidieren (double ticketPreis) {
		if (ticketPreis < 0) {
			ticketPreis = Math.abs(ticketPreis);
			System.out.println("\nDer Ticket Preis wurde von: " + ( - ticketPreis) + "\nAuf: " + ticketPreis + " geändert!\n");
		}
		return ticketPreis;
	}
}

/*
 * Datentyp für die Anzahl der Tickets: int Begründung: Jemand könnte 2147483646
 * Tickets an einem Automaten mit maximal 2 Euro stücken kaufen wollen.
 * Hauptsächlich aber ist es die kürzeste Declaration und damit schneller zu
 * schreiben.
 * 
 * Bei der Multiplikation von dem einzel Ticketpreis und der Anzahl wird eine
 * ganze Zahl mit einer Dezimalzahl multipliziert.
 */
