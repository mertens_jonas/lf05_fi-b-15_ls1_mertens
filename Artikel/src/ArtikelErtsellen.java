public class ArtikelErtsellen {

    public static void main(String[] args)
    {
        Artikel art1 = new Artikel();
        Artikel art2 = new Artikel();

        art1.setId(12);
        art1.setBezeichnung("Himalayareis");
        art1.setLagerBestand(412);
        art1.setMindestBestand(100);
        art1.setEinkaufsPreis(1.41);
        art1.setVerkaufsPreis(1.78);

        System.out.println("\nLagerbestand Artikel 1 vor 80% Auffüllung: " + art1.getLagerBestand());
        art1.artikelBestellen();
        System.out.println("Lagerbestand Artikel 1 nach 80% Auffüllung: " + art1.getLagerBestand());

        art2.setId(581);
        art2.setBezeichnung("Frische Fische von Fischers Fritze\t");
        art2.setLagerBestand(3);
        art2.setMindestBestand(10);
        art2.setEinkaufsPreis(7.21);
        art2.setVerkaufsPreis(12.45);

        System.out.println("\nLagerbestand Artikel 2 vor 80% Auffüllung: " + art2.getLagerBestand());
        art2.artikelBestellen();
        System.out.println("Lagerbestand Artikel 2 nach 80% Auffüllung: " + art2.getLagerBestand());

        System.out.println("\nGewinnmarge Artikel 1: " + art1.gewinnMargeBerechnen() + "€");
        System.out.println("Gewinnmarge Artikel 2: " + art2.gewinnMargeBerechnen() + "€");

        System.out.println("\nLagerbestand Artikel 2: " + art2.getLagerBestand());
        art2.lagerbestandVeraendern(301);
        System.out.println("Lagerbestand Artikel 2 nach +301: " + art2.getLagerBestand());

        Artikel art3 = new Artikel(723, "Mais", 3.12, 4.99, 50, 40);
    }
}
