import java.text.DecimalFormat;

public class Artikel {
    private int id;
    private String bezeichnung;
    private double einkaufsPreis;
    private double verkaufsPreis;
    private int lagerBestand;
    private int mindestBestand;

    public Artikel() {

    }

    public Artikel(int id, String bezeichnung, double einkaufsPreis, double verkaufsPreis, int lagerBestand, int mindestBestand) {
        this.id = id;
        this.bezeichnung = bezeichnung;
        this.einkaufsPreis = einkaufsPreis;
        this.verkaufsPreis = verkaufsPreis;
        this.lagerBestand = lagerBestand;
        this.mindestBestand = mindestBestand;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBezeichnung() {
        return this.bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public double getEinkaufsPreis() {
        return this.einkaufsPreis;
    }

    public void setEinkaufsPreis(double einkaufsPreis) {
        this.einkaufsPreis = einkaufsPreis;
    }

    public double getVerkaufsPreis() {
        return this.verkaufsPreis;
    }

    public void setVerkaufsPreis(double verkaufsPreis) {
        this.verkaufsPreis = verkaufsPreis;
    }

    public int getLagerBestand() {
        return this.lagerBestand;
    }

    public void setLagerBestand(int lagerBestand) {
        this.lagerBestand = lagerBestand;
    }

    public int getMindestBestand() {
        return this.mindestBestand;
    }

    public void setMindestBestand(int mindestBestand) {
        this.mindestBestand = mindestBestand;
    }
    public void artikelBestellen() {
        if (this.lagerBestand <= (this.mindestBestand * (80.0f / 100.0f))) {
            this.lagerBestand = this.mindestBestand;
        }
    }
    public double gewinnMargeBerechnen() {
        DecimalFormat df = new DecimalFormat("#.##");
        return Double.parseDouble(df.format((this.verkaufsPreis - this.einkaufsPreis)).replace(',', '.'));
    }

    public void lagerbestandVeraendern(int menge) {
        this.lagerBestand = this.lagerBestand + menge;
    }
}

