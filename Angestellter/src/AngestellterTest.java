public class AngestellterTest
{
    //------------Hauptprogramm---------------
    public static void main(String[] args)
    {
        Angestellter ang1 = new Angestellter();
        Angestellter ang2 = new Angestellter();

        //Setzen der Attribute
        ang1.setName("Hans");
        ang1.setNachname("Meier");
        ang1.setGehalt(4500);
        ang2.setName("Peter");
        ang2.setNachname("Petersen");
        ang2.setGehalt(6000);

        //Bildschirmausgaben
        System.out.println("Name: " + ang1.getName() + " " + ang1.getNachname());
        System.out.println("Gehalt: " + ang1.getGehalt() + " Euro");
        System.out.println("\nName: " + ang2.getName() + " " + ang2.getNachname());
        System.out.println("Gehalt: " + ang2.getGehalt() + " Euro");
        if (ang1.getGehalt() < ang2.getGehalt()) {
            System.out.println("\n" + ang2.getName() + " " + ang2.getNachname() + " verdient mehr als "  + ang1.getName() + " " + ang1.getNachname());
        }
    }//main
}//class