//Implementierung der Klasse “Angestellter”
public class Angestellter
{
    //Attribute
    private String name;
    private String nachname;
    private double gehalt;

    //Methoden
    public void setName(String name)
    {
        this.name = name;
    }
    public String getName()
    {
        return this.name;
    }
    public void setNachname(String nachname)
    {
        this.nachname = nachname;
    }
    public String getNachname()
    {
        return this.nachname;
    }
    public void setGehalt(double gehalt)
    {
        this.gehalt = gehalt;
    }
    public double getGehalt()
    {
        return this.gehalt;
    }
}
