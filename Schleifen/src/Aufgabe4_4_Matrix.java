import java.util.Scanner;
public class Aufgabe4_4_Matrix {
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
        int eingabe;
        boolean repeat = true;
        do {
            System.out.print("Geben sie eine Zahl zwischen 2 und 9 ein: ");
            eingabe = tastatur.nextInt();
            if (eingabe >= 2 && eingabe <= 9) {
                repeat = false;
            }
        } while (repeat);

        int zahl = 1;
        int counter = 1;
        boolean hit = false;
        System.out.print("0  ");
        do {
            hit = false;
            counter++;
            boolean divisible = zahl % eingabe == 0;
            if (divisible) {
                hit = true;
            }
            int checkDigits = zahl;
            while (checkDigits > 0) {
                if (checkDigits % 10 == eingabe){
                    hit = true;
                }
                checkDigits = checkDigits / 10;
            }

            int quersumme = 0;
            int checkQuer = zahl;
            while (checkQuer != 0) {
                quersumme = quersumme + (checkQuer % 10);
                checkQuer = checkQuer / 10;
            }
            if (quersumme == eingabe) {
                hit = true;
            }

            if (hit) {
                System.out.print("* ");
            } else {
                System.out.print(zahl);
                if (zahl < 10) {
                    System.out.print(" ");
                }
            }

            if (counter == 10) {
                System.out.print("\n");
                counter = 0;
            } else {
                System.out.print(" ");
            }
            zahl++;
        } while (zahl < 100);
    }
}
