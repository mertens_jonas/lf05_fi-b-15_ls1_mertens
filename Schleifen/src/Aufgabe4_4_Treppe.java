import java.util.Scanner;
public class Aufgabe4_4_Treppe {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Wie hoch soll die Treppenhöhe sein: ");
        int height = keyboard.nextInt();
        System.out.print("Wie breit sollen die Treppenstufen sein: ");
        int width = keyboard.nextInt();
        int span = width * height;
        for (int i = 0; i < height; i++){
            for (int leer = 0; leer <= span - width * i; leer++){
                System.out.print(" ");
            }
            for (int loop = 0; loop < i + 1; loop++){

                for (int b = 0; b < width; b++) {
                    System.out.print("*");
                }
            }
        System.out.print("\n");
        }
    }
}
