import java.util.Scanner;
public class Aufgabe4_4_Quersumme {
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
        System.out.print("Geben sie bitte eine Zahl ein: ");
        int eingabe = tastatur.nextInt();
        int quersumme = 0;
        while (eingabe != 0) {
            quersumme = quersumme + (eingabe % 10);
            eingabe = eingabe / 10;
        }
        System.out.print("Die Quersumme beträgt: " + quersumme);
    }
}
