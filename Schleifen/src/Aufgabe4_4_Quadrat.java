import java.util.Scanner;
public class Aufgabe4_4_Quadrat {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Wie lang soll die Kantenlänge des Quadrats sein: ");
        int length = keyboard.nextInt();
        int amount = 0;
        for (int i = 0; i < length; i++){
            for (int b = 0; b < length; b++){

                if (b == 0 || b == length -1) {
                    System.out.print("* ");
                } else if (i == 0 || i == length - 1) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.print("\n");

        }
    }
}
