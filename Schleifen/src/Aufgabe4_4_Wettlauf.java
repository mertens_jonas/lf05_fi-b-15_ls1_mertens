public class Aufgabe4_4_Wettlauf {
    public static void main(String[] args) {
        double distance = 1000.0;
        double speedA = 9.5;
        double speedB = 7.0;
        double A = 0.0;
        double B = 250.0;
        int seconds = 0;
        do {
            A = A + speedA;
            B = B + speedB;
            seconds = seconds + 1;
        } while (A < distance && B < distance);
        System.out.println("Distance A: " + A + "m");
        System.out.println("Distance B: " + B + "m");
        System.out.println("Zeit: " + seconds + "s");

        if (A > B){
            System.out.println("A hat gewonnen!");
        } else if (B > A) {
            System.out.println("B hat gewonnen!");
        } else {
            System.out.println("A und B waren gleich schnell!");
        }
    }
}
