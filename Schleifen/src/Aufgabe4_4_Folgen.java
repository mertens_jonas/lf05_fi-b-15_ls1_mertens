public class Aufgabe4_4_Folgen {
    public static void main(String[] args) {
        System.out.println("Aufgabe 4.4 For loop Aufgabe 4");
        System.out.print("a) ");

        for (int zahl = 99; zahl > 8; zahl = zahl - 3){
            System.out.print(zahl);
            if (zahl != 9){
                System.out.print(", ");
            }
        }

        System.out.print("\nb) ");

        int ergebniss = 1;
        for (int zahl2 = 3; ergebniss < 401; zahl2 = zahl2 + 2){
            System.out.print(ergebniss);
            if (ergebniss != 400) {
                System.out.print(", ");
            }
            ergebniss = ergebniss + zahl2;
        }

        System.out.print("\nc) ");

        for (int zahl3 = 2; zahl3 <= 102; zahl3 = zahl3 + 4){
            System.out.print(zahl3);
            if (zahl3 != 102) {
                System.out.print(", ");
            }
        }

        System.out.print("\nd) ");

        int zahlZwoelf = 12;
        for (int zahl4 = 4; zahl4 <= 1024; zahlZwoelf = zahlZwoelf + 8 ) {
            System.out.print(zahl4);
            if (zahl4 != 1024) {
                System.out.print(", ");
            }
            zahl4 = zahl4 + zahlZwoelf;
        }

        System.out.print("\ne) ");

        for (int zahl5 = 2; zahl5 <= 32768; zahl5 = zahl5 * 2) {
            System.out.print(zahl5);
            if (zahl5 != 32768) {
                System.out.print(", ");
            }
        }
    }
}
